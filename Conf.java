package cn.edu.neu.panda;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.io.File;
import java.io.IOException;
 
public class Conf {
public static void main(String[] args) {
String confName = "conf";
 /**
* this get the current directory
 */
   
 File currentDir = new File(".");
try {
System.out.println(currentDir.getCanonicalPath());
 } catch (IOException e) {
// TODO Auto-generated catch block
 //e.printStackTrace();
 }
      
       
Parse confParse = new Parse(confName);
 if(!confParse.isExist()) {
System.out.println("the program can not open the conf file, please check in");
 System.exit(1);
}
     
 HashMap<String, String> propertyMap = new HashMap<String, String>();
   
propertyMap = confParse.parseConf();
         
 if(!propertyMap.isEmpty()) {
Set<Map.Entry<String,String>> set = propertyMap.entrySet();
for(Entry<String, String> tempSet : set)  {
System.out.println(tempSet.getKey() + " = " + tempSet.getValue());
 }
 }
 }
}